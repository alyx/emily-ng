from configparser import ConfigParser
from os.path import expanduser

config = ConfigParser()
config.read(['emily.cfg', expanduser('~/emily.cfg')])