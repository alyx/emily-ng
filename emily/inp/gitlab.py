import asyncio

from emily.core import push_event, merge_event
from emily.protocols.http import http_handle, HTTPProtocol

class GitLab:
    """ Processes information from GitLab Web Hooks. """
    PROTOCOLS = (HTTPProtocol,)

    def __init__(self):
        http_handle.add(self.http_handle)

    @asyncio.coroutine
    def http_handle(self, request):
        if 'X-Gitlab-Event' not in request.headers:
            return False

        json = yield from request.json()
        if 'object_kind' not in json:
            return False  # why?!

        if json['object_kind'] == 'push':
            push_info = {}
            push_info['user'] = {'name': json['user_name'],
                                 'email': json['user_email']}
            push_info['branch'] = json['ref'].split('/')[-1]
            push_info['commits'] = []
            for commit in json['commits']:
                push_info['commits'].append({'id': commit['id'],
                                             'url': commit['url'],
                                             'msg': commit['message'],
                                             'author': commit['author']})

            project = request.match_info.get('grp', 'default') + '/' + \
                      request.match_info.get('project')

            yield from push_event.call_async(project, push_info)

            return True
        elif json['object_kind'] == 'merge_request':
            if json['object_attributes'].get('action', 'open') != 'open':
                return True

            merge_info = {}
            merge_info['user'] = {'name': json.get('user', {}).get('name', '')}
            merge_info['branch'] = json['object_attributes']['source_branch']
            merge_info['url'] = json['object_attributes']['url']

            project = request.match_info.get('grp', 'default') + '/' + \
                      request.match_info.get('project')

            yield from merge_event.call_async(project, merge_info)

            return True
        else:
            return False
